var refresh = 1000; // Refresh rate in milli seconds

function timer(){
	mytime=setInterval(timestamp, refresh);
}

function timestamp() {
	var x = new Date();
	var dateSec = x.getSeconds();
	var dateMin = x.getMinutes();
	var dateHour = x.getHours();
	var degSec = dateSec * 6;
	var degMin = dateMin * 6;
	var degHour = dateHour * 30; //multiply timestamp with degrees to get rotation
	seconds.style.setProperty('--degSec', degSec + 'deg');
	minutes.style.setProperty('--degMin', degMin + 'deg');
	hours.style.setProperty('--degHour', degHour + 'deg');
	timer();
	console.log(dateHour + " " + dateMin + " " + dateSec);
	console.log(degHour + " " + degMin + " " + degSec);	
}