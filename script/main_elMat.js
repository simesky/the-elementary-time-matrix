var timer = setInterval(construct, 1000);
var pause_btn = document.getElementById("pause_btn");
var cont_btn = document.getElementById("cont_btn");
var mainArr = [];
var userEl1 = prompt("What is your first element?");
var userEl2 = prompt("What is your second element?");
var userEl3 = prompt("What is your third element?");
var userEl4 = prompt("What is your fourth element?");
var paragraph = document.getElementById("para");
var slider1 = document.getElementById("textSizeSlider");

function text_uppercase() {
	paragraph.style.textTransform = "uppercase"
}

function text_lowercase() {
	paragraph.style.textTransform = "lowercase"
}

function text_none() {
	paragraph.style.textTransform = "none"
}

function pause_time() {
	clearInterval(timer);
	pause_btn.disabled = true;
	cont_btn.disabled = false;
	document.getElementById("para").style.animationPlayState = "paused";
	document.getElementById("title").style.animationPlayState = "paused";
}
function cont_time() {
	timer = setInterval(construct, 1000);
	pause_btn.disabled = false;
	cont_btn.disabled = true;
	document.getElementById("para").style.animationPlayState = "running";
	document.getElementById("title").style.animationPlayState = "running";
}

slider1.oninput = function(){
    paragraph.style.fontSize = this.value + "px";
    paragraph.style.letterSpacing = this.value / 4 - 0.5 + "px";
}

function construct() {
	for (i2 = 0; i2 < 1; i2++) {
		mainArr.push(userEl1);
		mainArr.push(userEl2);
		mainArr.push(userEl3);
		mainArr.push(userEl4);
		var i, j, k;
  			for (i = mainArr.length -1; i > 0; i--) {
    			j = Math.floor(Math.random() * i)
    			k = mainArr[i]
    			mainArr[i] = mainArr[j]
    			mainArr[j] = k
  			}
		var str1 = mainArr.toString();
		var text = str1.split(",");
		var str2 = text.join(" ");
		paragraph.textContent += str2 + "\r\n";
	}
}
