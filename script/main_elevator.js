var start_btn = document.getElementById('start_btn');
var patch;
var item1 = document.getElementsByClassName("item1");
var item2 = document.getElementsByClassName("item2");
var item3 = document.getElementsByClassName("item3");

$.get('sound/euclid_bossa2.pd', function(patchStr) {
	patch = Pd.loadPatch(patchStr);
	Pd.startOnClick(start_btn, function() {
    })
})

function start() {
	item1[0].style.animationPlayState = "running";
	item2[0].style.animationPlayState = "running";
	item3[0].style.animationPlayState = "running";
	start_btn.style.display = "none";
}

function standBy() {
	item1[0].style.animationPlayState = "paused";
	item2[0].style.animationPlayState = "paused";
	item3[0].style.animationPlayState = "paused";
}