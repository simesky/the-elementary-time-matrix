var sound_01 = document.getElementById("sound_01");
var sound_02 = document.getElementById("sound_02");
var volSlider = document.getElementById("volSlider");
var sim123_cover_orig = document.getElementById("sim123_cover_orig");
var sim123_cover_glitch = document.getElementById("sim123_cover_glitch");
			
function init() {
	sound_01.loop = true;
	sound_01.volume = 1;
	sound_01.preload = "auto";
	sound_02.loop = true;
	sound_02.volume = 0;
	sound_02.preload = "auto";
	sim123_cover_orig.style.opacity = 1;
	sim123_cover_glitch.style.opacity = 0;
}
function playAll() {
    sound_01.play();
    sound_02.play();
}
function pauseAll() {
    sound_01.pause();
    sound_02.pause();
}
volSlider.oninput = function(){
    sound_01.volume = 1 - this.value / 1000;
    sound_02.volume = this.value / 1000;
	sim123_cover_orig.style.opacity = (1-this.value/1000);
	sim123_cover_glitch.style.opacity = this.value/1000;
}