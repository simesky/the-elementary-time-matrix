var refresh = 60000;

function timer() {
    mytime=setTimeout('add_cells()', refresh);
}

function add_cells() {
    var x = new Date();
    // var dateSec = x.getSeconds();
    var dateMin = x.getMinutes();
    var dateHour = x.getHours();
    var maxCell = dateMin;
    var modulo = dateHour;
    var _cellBlack = '<div class="black"></div>',
        _cellWhite = '<div class="white"></div>';
    var _gridContainer = document.querySelector('#container');
    for(var i = 0; i < maxCell; i++) {
        for(var j = 0; j < maxCell; j++) {
            if((i + j) % modulo == 0) {
                _gridContainer.insertAdjacentHTML('beforeend', _cellBlack);
            } else {
                _gridContainer.insertAdjacentHTML('beforeend', _cellWhite);
            }
        }
    }
    console.log(modulo + " " + maxCell);
    timer();
}