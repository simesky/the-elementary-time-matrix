var sound_01 = document.getElementById("sound_01");
var start_btn = document.getElementById("start_btn");
var circle = document.getElementById("circle");

function start() {
    sound_01.play();
    start_btn.style.display = "none";
    circle.style.animationPlayState = "running";
}

function standBy() {
	circle.style.animationPlayState = "paused";
}