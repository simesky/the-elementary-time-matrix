var i = 0;
function loading() {
  if (i == 0) {
    i = 1;
    var bar = document.getElementById("bar");
    var width = 0.1;
    var id = setInterval(frame, 20); /*time of progression steps in ms*/
    var width_rate = 1;
    function frame() {
      if (width >= 100) {
        clearInterval(id);
        i = 0;
      } else {
      	width_rate += 0.05;
        width += 1 / width_rate;
        bar.style.width = width + "%";
      }
    }
  }
}